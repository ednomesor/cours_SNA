READ ME
=====================================

Le dépôt est constitué de trois dossiers contenant les scripts, supports de cours et les présentations pour la formation SNA donnée à : 

* Quantilille 2017
* Ecole thématique CNRS - Initiation à l'analyse de réseau du GDR ARSHS - Nice 2-8 juillet 2017

## Branches
Les présentations ont été regroupées sous forme de branches dans le dépôt GIT. A chaque branche correspond donc l'ensemble des fichiers nécessaires pour une formation.

## Licence
Les présentations et supports de cours créés par moi-même sont sous Licence CC0 - Domaine public. 
Toutefois, j'utilise dans mes scripts des données extérieurs, pour la plupart provenant d'articles scientifiques publiés. Prière donc de respecter les droits d'auteur liés à ces travaux.

