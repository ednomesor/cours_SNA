---
title: "Initiation à l'analyse de réseaux avec R - Séance 1"
subtitle: "Quantilille"
author: "R. Letricot"
date: "27 juin 2017 | matin"
output: pdf_document
fontsize: 12pt
geometry: margin=1.2in
toc: true

---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Avant-propos
Ce document a été conçu en support à la séance *Initiation à l'analyse de réseaux* pour la formation Quantilille 2017. Dans cette séance, nous serons amenés à manipuler les logiciels R et RStudio ainsi que les packages **igraph**..

Ce document rédigé en R Markdown a l'avantage de mêler texte, commande R et visualisation sur un seul et même document. 

L'export PDF et le fichier source Rmd sont disponibles sous licence CC0 (<https://creativecommons.org/publicdomain/zero/1.0/deed.fr>), c'est-à-dire libre de droit, pour la simple et bonne raison qu'ils ne sont pas de ma seule et unique facture mais inspirés de nombreuses ressources disponibles en ligne. Si vous réutilisez le contenu de ce cours, rien ne vous empêche d'avoir la gentillesse de citer ce document, de la même manière que je cite les ressources qui ont inspiré l'écriture de ce travail dans la section **Ressources utiles**. Mais ce n'est en rien une obligation...

Les données que nous allons manipuler peuvent, quant à elles, faire l'objet de licence spécifique. 

L'ensemble des données, scripts et présentations que j'ai produit pour la formation Quantilille est disponible sur le dépôt GIT : 

## Ressources utiles
Laurent Beauguitte, Analyser les réseaux avec R (packages statnet, igraph et tnet). DEA. Analyser les réseaux avec R, UMR Géographie-cités, 2012, pp.17. Disponible sur HalSHS <https://cel.archives-ouvertes.fr/cel-00687871/document>

Julien Barnier, \textit{Introduction à R}, livret de formation, dernière version disponible sur : <http://alea.fr.eu.org/pages/intro-R>.

Julien Barnier, *Analyse de réseau avec R*, livret de formation, 2011 : 
<https://alea.fr.eu.org/git/doc_reseaux_r.git/blob_plain/HEAD:/networks.pdf>

Tutoriel sur igraph produit par l'un de ses développeurs <http://igraph.github.io/netuser15/user-2015.html#1>

Concernant les différentes méthodes de détection de communautés avec igraph : 
<https://stackoverflow.com/questions/9471906/what-are-the-differences-between-community-detection-algorithms-in-igraph>

Egalement pour une version précédente de igraph :
<https://www.r-bloggers.com/summary-of-community-detection-algorithms-in-igraph-0-6/>

## 1 - Introduction
### Présentation de R

R est un langage utilisé pour le traitement de données et l'analyse statistique. Il est développé sous licence GNU GPL\footnote{}. Multiplateforme, il s'installe sur de nombreux systèmes d'exploitation notamment Windows, Mac et les environnements Linux. Quand on télécharge R, on installe un noyau de fonctionnalités appelé \textit{core} auquel on ajoute des extensions supplémentaires, appelées \textit{packages}, qui vont permettre d'effectuer un panel d'actions spécifique ou optimisé pour la manipulation des certains objets. R et ses  \textit{packages} sont développés par une communauté internationale de contributeurs. Une fois à l'aise avec l'environnement R, rien ne vous empêche de coder votre propre package R qui répondra à un besoin spécifique pour lequel vous n'aviez pas trouvé d'extension satisfaisante. Ce que je veux dire par là, c'est qu'il existe une multitude de \textit{packages} parfois redondants, maintenus dans le temps ou non.
Pour s'orienter dans cette nébuleuse, il existe un répertoire des \textit{packages} publié sur le site du \textit{Comprehensive R Archive Networkd (CRAN)} <http://cran.r-project.org>.


S'il a beaucoup d'avantages (libre, gratuit, multiplateforme, puissant, maintenu activement par une communauté), il a l'inconvénient majeur de ne pas être très ergonomique et graphique pour l'utilisateur débutant. R se rapproche davantage du langage de programmation et fonctionne à l'aide de scripts composés de différentes commandes et de fonctions de traitement.
Ces scripts vont être exécutés dans une console et produire des résultats sous forme chiffrée ou graphique (tableau, histogramme, réseau, etc.). Il n'y a pas de bouton permettant d'exécuter des fonctions pré-définies comme dans un logiciel à interface graphique traditionnel. Il va falloir écrire soi-même toutes les manipulations que l'on veut exécuter sur ces données. A l'instar d'une recette de cuisine, vous devrez spécifier les ingrédients, les outils utilisés et le type de transformation que vous allez leur faire subir pour arriver à un résultat final plus ou moins comestible selon vos talents de cuisinier. 

Pour continuer sur cette analogie, le format d'une recette de cuisine est assez standard : en préambule vous retrouvez le niveau de difficulté, le temps de préparation et le temps de cuisson, puis vient la liste des ingrédients, et enfin les étapes détaillées de leur préparation. Cette formalisation facilite la compréhension de la recette mais aussi sa transmissibilité (on peut vouloir rechercher une recette avec un temps de préparation court, ou avec seulement certains ingrédients).
Pour construire un script R, cela se passe de la même façon, il faut tout détailler. De cette manière, à structure de données également, n'importe qui pourra exécuter votre script avec son propre jeu de données. Par ailleurs, l'explicitation des commandes à l'aide de commentaires est importante afin de permettre aux autres et à vous-même de comprendre ce qu'il se passe dans le script.

Mais au fait, pourquoi s'embêter à écrire un script ?!
L'avantage de formaliser ses traitements dans un script est, qu'une fois votre script rédigé ,vous n'avez plus à vous soucier d'effectuer une par une les manipulations (ouvrir un fichier, charger, modifier les données, opérer les calculs, exporter les résultats etc.). Tout sera consigné dans votre script. Si la structuration des données est respectée, le script exécutera les traitements que vous avez codés qu'importe la valeur de vos données de départ. Pratique !

### L'interface RStudio
L'interface de RStudio se compose d'un espace de travail en 4 blocs : le script en cours de rédaction (en haut à gauche), la console (en dessous), l'environnement de variables (en haut à droire), l'espace d'affichage des visualisations avec les onglets pour atteindre les dossiers, la liste des packages installés et/ou actifs dans R, l'aide, etc. (en bas à droite). 

Un script s'exécute : 
* soit ligne par ligne avec le bouton RUN ou au clavier Ctrl+Enter (exécutera la commande là où se situe votre pointeur)
* soit intégralement en sélectionnant tout le script puis en cliquant sur le bouton RUN, au clavier Ctrl+Shift+Enter, ou dans la console `source("myScript.r", echo = TRUE)`

### Bonnes pratiques

* Il est préférable de créer un projet R (sorte de dossier dédié à votre manipulation) où vous pourrez enregistrer vos scripts, vos exports mais aussi les données des manipulations en cours. C'est très pratique quand vous devez interrompre votre travail sur R mais que vous souhaitez garder active les données de votre environnement de variable.
* Commentez votre code à l'aide du # (indiquer le nom, la date et le contexte en premières lignes permet de se remémorer ce que l'on cherchait à produire)
* Appeler les packages nécessaires à l'analyse en début de script

### Trouver de l'aide dans R

## 2 - Créer manuellement un graphe dans R

Ouvrir le script : script2_donnees_int.R
Notez l'utilisation du package igraph qui permet de créer et de manipuler des graphes selon une syntaxe propre à ce package.

Ici, nous créons un graphe, en indiquant la liste des arêtes et la distribution des liens du réseau. "n = 3" permet de spécifier le nombre de noeuds.

```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
# Quantilille
# Séance : Initiation à l'analyse de réseau
# Date : 27 juin 2017
# Auteur : Rosemonde Letricot

# Première manipulation de réseaux créés soi-même avec R
#Je nettoie mon espace de travail
rm(list=ls())

#Je configure mon chemin de fichier
setwd("~/Documents/framagit/cours_SNA/script_R/")

#Je charge mon package
library(igraph)

#Je crée un graph simple à trois sommets
g1 <- graph(edges=c(1,2, 2,3, 3,1)) 
#Voir mon graphe
plot(g1) 
```

Un graphe simple est un graphe qui ne contient pas de boucles (liens vers un même sommet) et pas de liens multiples (plusieurs liens vers deux sommets)

Pour savoir si le graphe est simple ou non, on utilise dans igraph la fonction `is_simple`

```{r}
is_simple(g1)
```

**La typologie des graphes :** 

* Graphe simple : Pas de boucle, pas de liens multiples
  + orienté : liens directionnels entre les noeuds
  + non orienté : lien considéré comme réciproque
  + valué : graphe dont les liens ont un poids différent marqué par une valeur numérique
  + complet : graphe dont tous les liens possibles sont présents
  
* Multigraphe : graphe ayant plusieurs liens de nature différente entre un couple de noeuds

* Graphe bipartite (2-mode) : composé de liens entre deux ensembles de sommets différents

* Graphe complexe : si son évolution fait émerger des propriétés non prévues au départ. Il peut être de type *petit monde* ou *scale free*.

### Graphe orienté ou non orienté
A chaque fois, il est faut spécifier si le graphe en question (créé manuellement ou importé depuis un fichier extérieur) est orienté ou non. Par défaut, igraph considérera que le graphe est orienté (valeur `directed = T` par défaut)

Un graphe orienté est constitué de noeuds reliés entre eux par des liens directionnels (ou bidirectionnels) qui précisent que la relation part d'un noeud pour se diriger vers un autre, la réciprocité n'est pas nécessairement présente. Ex : Anne offre un café à Christophe mais Christophe n'a pas offert de café à Anne - Quel goujat !. Là, la relation est unidirectionnelle.
Si Christophe a finalement offert un café à Anne, j'aurais indiqué la réciprocité de cette relation par un lien orienté d'Anne vers Christophe, puis de Christophe vers Anne. Mais si je m'interesse seulement au fait qu'Anne et Christophe ont eu un échange à la machine à café qu'importe la direction du don, j'aurais créé un lien non orienté entre ces deux acteurs. Tout dépend de ce que l'on cherche à démontrer.

On va créer un graphe orienté dans igraph (je précise le nombre de sommets totaux avec `n =` )

```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
#Je crée un graphe orienté à 8 sommets
g2 <- graph(edges=c(1,1, 1,2, 1,2, 2,3, 3,4, 3,1, 4,2, 4,1, 4,1), n=8, directed=TRUE) 
#Voir mon graphe
plot(g2) 
```

### Graphe simple, multiple et fonction pour simplifier un graphe

Est-ce un graphe simple, fonction `is_simple(nomdugraphe)` :
```{r}
is_simple(g2)

```

Est-ce un graphe multiple, fonction `is_multiple(nomdugraphe)` : 
```{r}
is.multiple(g2)
```

Pour le simplifiez, fonction `simplify(nomdugraphe)` :
```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
g2<-simplify(g2)
plot(g2)

#Est-ce un graphe simple maintenant ?
is_simple(g2)
g2
```


### Créer un graphe avec des noms
```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
#Au lieu d'avoir des numéros je peux avoir des noms
g3 <- graph( c("Pierre", "Paul", "Paul", "Anne", "Anne", "Pierre")) 
# Quand les liens ont des noms, il n'est pas nécessaire de préciser le nombre de sommets
#Voir mon graphe
plot(g3)
```

### Spécifier les noeuds isolés

Le paramètre `isolates` de la fonction `graph()` permet d'indiquer le nom des noeuds isolés dans un graphe nommé.

```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
g4 <- graph( c("Pierre","Paul" , "Paul","Eric" , "Pierre","Eric" , "Paul","Marc" , "Marc","Eric" , "Marc","Pierre"), isolates=c("Anne", "Greta", "Gabrielle", "Emilie"), directed = TRUE )  

plot(g4)

```

### Ajouter des noeuds ou des liens

```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
g4bis <- g4 + 
  vertices("Bob") +
  edge("Bob", "Greta")
plot(g4bis)
```


### Encore une autre façon de formaliser un graphe
Cette formalisation utilise les signes `+` et `-` pour caractériser les relations entre les noeuds. Dans le cas d'un graphe orienté, `+-` et `-+` pour l'orientation du lien, `+` pour la réciproque. 
Si le nom du noeud contient un espace ou un caractère spécial on utilise la double quote (c'est le cas ici de Marie Louise). 
On utilise la virgule pour créer un isolé.

```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
#Une autre façon encorede produire un graphe, notez l'utilisation de ""
g3bis <-graph_from_literal(Pierre-Paul-Anne-Pierre-"Marie Louise", André)
#Voir mon graphe
plot(g3bis)
```

### Focus sur la fonction plot()

La fonction `plot()` est une fonction native de R, mais l'extension igraph lui a ajouté de nombreux paramètres dédiés à la visualisation de graphe.
Pour connaître l'ensemble des paramètres disponibles pour cette fonction voir la documentation igraph : 
<http://igraph.org/r/doc/plot.common.html>

```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
#Créer une visualisation et la personnaliser
plot(g4, edge.arrow.size=0.5, 
     vertex.color="red", 
     vertex.size=15, 
     edge.color="grey20", 
     vertex.label.color="blue", 
     vertex.label.cex=0.8, 
     vertex.label.dist=0.2, 
     edge.curved=0.2) 
```

## 3 - Manipuler mon objet igraph

Quand je crée un graphe manuellement ou quand je charge un jeu de données réseaux avec le package `igraph`, j'initialise un objet igraph doté de certaines caractéristiques. Pour les connaître, je demande à R des informations sur l'objet igraph que je viens de créer/charger :
```{r}
#Info générale sur mon objet igraph
g4
```

Il renvoie une sorte de résumé, comprenez : 

* `IGRAPH` : c'est un objet igraph (jusque là tout va bien !)

* `DN--` : D pour *directed graph* (sinon U pour *undirected*), N pour *named graph* c'est à dire qu'igraph à reconnu que les noeuds portaient des noms, les deux autres lettres (absentes ici) seraient W pour *weighted graph* si les liens sont valués et B pour *bipartite graph* si le graph est biparti (2-mode).

* 8 correspond à l'ordre d'un graphe, c'est-à-dire le nombre de ses noeuds

* 6 correspond à sa taille, c'est-à-dire au nombre de ses liens

* à la suite des deux tirets serait inscrit le nom du graphe s'il en a un

* `attr: ` : donne la liste des attributs du réseau, ici seul `name (v/c)` indique qu'il y a un attribut name pour les noeuds (*vertices* en anglais) de type c *character*.

On peut vouloir des informations propres aux sommets ou aux liens. E pour *Edges* (liens), V pour *Vertices* (noeuds).

```{r}
#lister les arrêtes
E(g4)

#lister les sommets
V(g4)
```

...ou examiner mon réseau sous forme de matrice :
```{r}
g4[]
```


### Focus sur les attributs des liens et des sommets

Pour examiner le contenu des attributs des noeuds comme des liens, on utilise le signe dollar `$`, en précisant si l'on s'intéresse aux noeuds `V(nomdugraphe)` ou aux liens `E(nomdugraphe)`
```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
#R automatiquement va assigner la colonne $name lors de la création du graphe
#Donc si je lui demande la liste des noms
V(g4)$name

```

Avec cette syntaxe `V(nomdugraphe)$nomattribut`, on peut manipuler cette objet comme n'importe quel objet R et vouloir lui assigner des variables :
```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
#Ajoutons des attributs à nos sommets
#J'ajoute des données sur le genre
V(g4)$genre <- c("homme", "homme", "homme", "homme", "femme", "femme", "femme", "femme")
```

On peut aussi le faire pour les liens `E(nomdugraphe)$nomattribut`
```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
# Valuons notre réseau
E(g4)$weight <- c(3,5,1,4,8,4)

```

Cette syntaxe peut être également utilisée dans les paramètres de la fonction `plot()` pour personnaliser la visualisation selon la valeur des attributs présents dans le graphe.

```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
plot(g4, 
     edge.arrow.size=.5, 
     vertex.label.color="black", 
     vertex.label.dist=1.0,
     vertex.color= ifelse(V(g4)$genre=="homme", "skyblue", "pink")
     ) 
```

Pour avoir l'ensemble des attributs disponibles
```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
list.vertex.attributes(g4)
list.edge.attributes(g4)
```


### Exercice de manipulation n°1

Dans un nouveau script, 

* Créer un graphe non orienté, complet, d'ordre 5

* Nommer vos noeuds

* Incruster Bob dans le réseau entre deux noeuds

* Valué votre graphe en donnant la plus forte valeur entre Bob et un de ses voisins

* Vérifier que l'objet igraph est bien valué

* Produire une visualisation où le poids du lien le plus fort est en rouge


## 4 - Créer un graphe à partir d'un jeu de données importé

Maintenant que nous sommes un peu plus à l'aise avec la manipulation d'objet igraph, on va pouvoir traiter des données extérieures.

### Importer une liste de liens
Ouvrir le fichier script3_donnees_ext.R

Dans ce script, nous allons travailler à partir de données contenues dans des fichiers CSV du dossier `/data`

Nous allons créer un graphe avec la liste de liens du fichier "edges_anonym.cvs". 
On charge nos données dans un dataframe appelé `liens`.
La fonction `head(nomdelavariable)` nous permet de jeter un rapide coup d'oeil sur son contenu et de prendre connaissance des différents attributs que l'on aimerait associer au graphe.

`igraph` en déduira la liste des noeuds et les nommera automatiquement.

```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
#Quantilille
# Séance : Initiation à l'analyse de réseau - 
# Première visualisation de réseau à partir d'un jeu de données externe
# Date : 27 juin 2017
# Auteur : Rosemonde Letricot

#Je nettoie mon espace de travail
rm(list=ls())

setwd("~/Bureau/Formation/Nice_GDR/nice1/script_R/")

#J'active mes extensions
library(igraph)
library(colorRamps)

#Je charge mes données
liens<-read.csv("~/Documents/framagit/cours_SNA/script_R/data/edges_anonym.csv",sep=",", header=TRUE)
head(liens)

#Je crée un graphe avec mes données
g<-graph.data.frame(liens, directed = FALSE)
#Avoir des infos sur mon graphe
g

#On peut customiser la présentation de ce réseau
plot(g, 
     vertex.size=5, 
     vertex.color="red",
     vertex.label.cex=0.5,
     vertex.label = NA
     #vertex.label.dist=0.5
     )
```

### Importer une liste de noeuds
Dans le dossier `/data`, nous avons un fichier nodes_anonym.csv que nous souhaitons utiliser pour charger les données attributaires de noeuds de notre réseau.

Tout d'abord, on le charge dans un dataframe.

```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
noeuds<-read.csv("~/Bureau/Formation/Nice_GDR/nice1/script_R/data/nodes_anonym.csv", sep = ",", header = TRUE)
head(noeuds)
```

```{r}
liens<-read.csv("~/Bureau/Formation/Nice_GDR/nice1/script_R/data/edges_anonym.csv", sep = ",", header = TRUE)
head(liens)
```

Nous allons créer un nouvel objet graphe (ou plutôt écraser l'ancien) qui importera à la fois les données sur les liens et celles sur les noeuds que je viens de mettre respectivement dans deux dataframes différents.
```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
#transformation en objet igraph
net<- graph_from_data_frame(d=liens, vertices=noeuds, directed = F)

```


### Manipuler son graphe, parcourir les données attributaires

Petit rappel ici sur la manière de parcourir les attributs (noeuds/liens) que nous avons vu dans la partie précédente. 

```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
class(net)
(net)

# E pour inspecter les arêtes du réseau
E(net)

# V pour inspecter les sommets du réseau
V(net)

#S'il y a des données attributaires arêtes/sommets on peut y accéder de cette façon
E(net)$weight
V(net)$sexe
V(net)$code
```

On peut effectuer quelques requêtes sur les attributs à l'aide d'opérateurs arithmétiques ou logiques

En voici quelques uns : 
`>` strictement supérieur
`<` strictement inférieur
`>=` supérieur ou égal
`<=` inférieur ou égal
`!=` différent
`==` égal ( oui il faut mettre `==` et pas `=`)

`!` négation 
`&, &&` et
`|, ||` ou inclusif (retournera VRAI si l'une ou l'autre ou les deux propositions sont vraies)
`xor(,)`ou exclusif (retournera VRAI si l'une ou l'autre mais pas les deux propositions sont vraies)
 

```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
#Chercher des sommets ou des liens en fonction de la valeur des attributs
E(net)[weight>39]
V(net)[sexe=="f"]
V(net)[code=="TARG" | code=="LO"]
```

On peut visualiser le graphe selon la valeur des attributs

```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
plot(net,
     vertex.size=5,
     vertex.color=ifelse(V(net)$sexe=="m", "skyblue", "pink"),
     vertex.label = NA
     )

#création d'une palette en fonction du nombre de valeurs différentes dans l'attribut code du réseau
pal<-colorRampPalette(c("blue", "red", "grey", "orange", "yellow", "pink", "purple", "green"))(length(unique(V(net)$type)))
V(net)$color<-pal[V(net)$type]
#Visualisation avec noeuds de couleur différente selon la valeur de l'attribut code
plot(net,
     vertex.size=5,
     vertex.color=V(net)$color,
     vertex.label = V(net)$code
)
```

### Simplifier un graphe : dédoublonner, enlever les boucles
```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
#####Les Liens#####
#Inspecter s'il y a des doublons
nrow(liens)
nrow(unique(liens[,c("source", "target")]))

#Enlever les doublons
liens2<-unique(liens)
#Agréger les doublons
liens3<-aggregate(liens[,3], liens[,-3], sum)
colnames(liens3)[3]<-"weight"


#Faire un nouvel objet igraph avec la liste des liens agrégés créés au-dessus
net<- graph_from_data_frame(d=liens3, vertices=noeuds, directed = F)

#OU
#Travailler directement à partir de l'objet graphe
is_simple(net)
net<-simplify(net, remove.loops = TRUE)
is_simple(net)
```

Personnaliser la représentation des liens selon leurs attributs
```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
#Visualisation modifiant l'épaisseur des liens
plot(net,
     vertex.size=5,
     vertex.color=c( "pink", "skyblue")[1+(V(net)$sexe=="m")],
     vertex.label = NA,
     edge.color = "grey",
     edge.width = 1+E(net)$weight/10
     )
```

###Les algorithmes de visualisation

```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
#Les layouts
#random
l <- layout_randomly(net)
plot(net,
     vertex.size=5,
     vertex.color=c( "pink", "skyblue")[1+(V(net)$sexe=="m")],
     vertex.label = NA,
     edge.color = "grey",
     edge.width = 1+E(net)$weight/10,
     layout = l
)

#En cercle
l <- layout_in_circle(net)
plot(net,
     vertex.size=5,
     vertex.color=c( "pink", "skyblue")[1+(V(net)$sexe=="m")],
     vertex.label = NA,
     edge.color = "grey",
     edge.width = 1+E(net)$weight/10,
     layout = l
)

#Fruchterman-Reingold
l <- layout_with_fr(net)
plot(net,
     vertex.size=5,
     vertex.color=c("pink", "skyblue")[1+(V(net)$sexe=="m")],
     vertex.label = NA,
     edge.color = "grey",
     edge.width = 1+E(net)$weight/10,
     layout = l
)

V(net)$color <-ifelse(V(net)$sexe == "m", "skyblue", "pink")

```
Les layouts disponibles avec le package igraph
```{r}
#Les différents layouts disponibles
layouts <- grep("^layout_", ls("package:igraph"), value=TRUE)[-1] 
# Remove layouts that do not apply to our graph.
layouts <- layouts[!grepl("bipartite|merge|norm|sugiyama|tree", layouts)]
par(mfrow=c(3,3), mar=c(1,1,1,1))
for (layout in layouts) {
  print(layout)
  l <- do.call(layout, list(net)) 
  plot(net, edge.arrow.mode=0, layout=l, main=layout, vertex.label = NA, vertex.size=4) }

```

### Exercices de manipulation n°2

## 5 - Exporter

### Mettre un titre et une légende à sa visualisation

```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
#Visualisation modifiant l'épaisseur des liens
plot(net,
     vertex.size=5,
     vertex.color=c( "pink", "skyblue")[1+(V(net)$sexe=="m")],
     vertex.label = NA,
     edge.color = "grey",
     edge.width = E(net)$weight/10
     )

#Mettre un légende
legend(x = 1.0, y=-1.3, c("Homme", "Femme"), col="#777777", pch = 21 , pt.bg=V(net)$color, cex=.8, ncol=1)
title(main = "Un réseau fait d'hommes et de femmes", cex.main = 1.0, col.main = "grey20")

```

### Exporter les visualisations et les données
```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}

net<- graph_from_data_frame(d=liens3, vertices=noeuds, directed = F)

png("~/Bureau/Formation/Nice_GDR/nice1/script_R/export/test.png")
l <- layout_with_fr(net)
plot(net,
     vertex.size=5,
     vertex.color=c("pink", "skyblue")[1+(V(net)$sexe=="m")],
     vertex.label = NA,
     edge.color = "grey",
     edge.width = 1+E(net)$weight/10,
     layout = l
)

V(net)$color <-ifelse(V(net)$sexe == "m", "skyblue", "pink")

#Mettre un légende
legend(x = -1.5, y=-1.1, c("Homme", "Femme"), col="#777777", pch = 21 , pt.bg=V(net)$color, cex=.8, ncol=1)
dev.off()


write_graph(net, "~/Bureau/Formation/Nice_GDR/nice1/script_R/export/monreseau.txt", format = "edgelist")

```

## 7 - Exercice de synthèse

A faire

## Corrections des exercices

### Exercice 1

```{r, tidy=TRUE, tidy.opts=list(width.cutoff=60), fig.align='center', fig.height=4}
soluce <- make_full_graph(5, directed = F)
V(soluce)$name<-c("SPO", "NGE", "SQU", "ARE", "PANTS")
soluce <- soluce + vertices("BOB") + edge("BOB", "NGE", "BOB", "SQU")
E(soluce)
E(soluce)$weight<-c(1,2,2,3,2,3,2,1,2,2,8,7)
soluce

plot(soluce, 
     edge.color=ifelse(E(soluce)$weight>=8, "red", "grey")
     )
```

